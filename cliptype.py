import pyperclip
from pynput.keyboard import Key, Controller
from time import sleep


def return_text_replaced(s_text):
    s_text_replaced = s_text.replace("\n", "|")
    s_text_replaced = s_text_replaced.replace("\t", "/")

    return s_text_replaced


def print_menu():

    b_slash_r = False
    s_warning = ""

    if "\r" in s_text_in_memory:
        b_slash_r = True
        s_warning = " - Warning Windows slash r characters present. Remove slash r recommended"

    s_text_replaced = return_text_replaced(s_text_in_memory)

    print("ClipType v. 1.1 - Menu")
    print("======================")
    print()
    print("1. Copy actual clipboard to memory")
    print("2. Show what's in the memory to be typed (", len(s_text_in_memory), "bytes)" + s_warning)
    print("3. Show what's in the memory to be typed (", len(s_text_replaced), " bytes) replacing Enter by | and tab by / for viewing" + s_warning)
    print("4. Remove slash r")
    print("5. Send to focused app after " + str(i_time) + " seconds")
    print("6. Set time (" + str(i_time) + " seconds)")
    print()
    print("0. Exit")

    if b_slash_r is True:
        print()
        print("Windows uses \\r \\n for Enter. If \\r are not removed you will get double Enters when sending the keystrokes.")
        print()


def ask_for_option(s_message, i_min, i_max):
    while True:
        s_answer = input(s_message)
        try:
            i_answer = int(s_answer)
            if i_answer >= i_min and i_answer <= i_max:
                return i_answer
        except:
            print("Values should be between", i_min, "and", i_max)


if __name__ == "__main__":

    s_text_in_memory = ""
    i_time = 10
    o_keyboard = Controller()

    try:

        while True:
            print_menu()
            i_option = ask_for_option("Select:", 0, 6)

            if i_option == 0:
                exit()

            if i_option == 1:
                try:
                    s_text_in_memory = pyperclip.paste()
                except pyperclip.PyperclipException:
                    print("Cannot get the Clipboard!. If you're under Linux you need to install one of these:")
                    print("""                
                    sudo apt-get install xsel      to install the xsel utility.
                    sudo apt-get install xclip     to install the xclip utility.
                    pip install gtk                to install the gtk Python module.
                    pip install PyQt4              to install the PyQt4 Python module.
                    """)
                print("Len of text copied in memory is:", len(s_text_in_memory))
                print()

            if i_option == 2:
                print(s_text_in_memory)

            if i_option == 3:
                s_text_replaced = return_text_replaced(s_text_in_memory)
                print(s_text_replaced)

            if i_option == 4:
                s_text_in_memory = s_text_in_memory.replace("\r", "")

            if i_option == 5:
                print("Going to send to keyboard in " + str(i_time) + " seconds. Set focus to the target application")
                for i_counter in range(i_time, 0, -1):
                    print(i_counter)
                    sleep(1)

                print("Sending", len(s_text_in_memory), "bytes of data...")

                o_keyboard.type(s_text_in_memory)
                print()

            if i_option == 6:
                try:
                    s_time = input("Enter time:")
                    i_time = int(s_time)
                except:
                    print("Invalid time")

    except KeyboardInterrupt:
        print("Exiting")
