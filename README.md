# clipbtype

By Carles Mateo

https://blog.carlesmateo.com/cliptype

First you need to install Python, pip, and then install the required Python packages:

* pyperclip
* pynput

Or on Ubuntu just do:

`pip3 install -r requirements.txt`

In Linux/Mac:

`pip install -r requirements.txt`

If you use Linux you'll need to install one of these utilities:

                sudo apt-get install xsel      to install the xsel utility.
                sudo apt-get install xclip     to install the xclip utility.
                pip install gtk                to install the gtk Python module.
                pip install PyQt4              to install the PyQt4 Python module.
